"""Run main application."""

from mbta.cli import run_cli


def run():
    run_cli()


if __name__ == '__main__':
    run()
