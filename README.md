# MBTA Predict

Command line interface to predict next train departure time from a chosen stop.

## Dependencies

- Python 3.6+
- Python libraries as indicated in requirements file

---

## Quickstart

Clone the project: `git clone git@bitbucket.org:csansone/mbta.git`

### Virtual Environment

Create a virtualenv to run the project, using your favorite method. If you don't have a process for this, one way is to use `pyenv` as described in [this tutorial](https://realpython.com/intro-to-pyenv/).

### PIP Install Libraries

Be sure you have activated your virtualenv from above.

```
pip install -r requirements.txt
```

### API Key (Optional)

**Do I need an API key?** Not strictly. Published API rate limits are 20 calls/min without key, raised to 1000 calls/minute with valid key.

You may be able to stay below the limit if only getting a prediction or two at a time. If you choose to try this first, skip ahead to the next section.

**Get API key** by registering on the developer portal [here](https://api-v3.mbta.com/)

**Once you have your key**, save it in a file named `mbta_api_key` under your home directory.

You should be able to `cat ~/mbta_api_key` and see the key.

### Run it

```
python -m run
```

Depending on your system, you may need to use `python3` instead of `python` to run 

---

## Testing

Use a separate virtualenv to run tests. As above, this is kind of a user-specific workflow.

Install dependencies (flake8, pytest) with:

```
pip install -r requirements-test.txt
```

Run the tests:

```
flake8 .

# errors will print, or no feedback if no errors

python -m pytest tests

# results will print
```

---

## Possible Improvements

Short list of obvious things that could be done better, but were omitted in the interest of time:

- LRU cache with time expiration on `get_routes` and `get_stops`, which don't change frequently
- Pagination not needed at this scope, but could be for different use
- More interesting error handling
- API call mocks for tests
- Better time handling