# session.py

from pathlib import Path

import requests


API_KEY_FILENAME = 'mbta_api_key'  # filename within user's home directory to have text file


def get_api_key():
    """Return string representation of API key.

    Could be expanded to use env var or alternate file locations if demand presents itself.
    """
    if not Path.home().joinpath(API_KEY_FILENAME).is_file():
        return None
    return Path.home().joinpath(API_KEY_FILENAME).read_text().strip()


def create_session():
    api_key = get_api_key()
    session = requests.Session()

    if api_key is not None:
        session.headers.update({'x-api-key': api_key})

    return session
