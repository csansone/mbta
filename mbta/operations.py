# operations.py

from datetime import datetime


BASE_API_URL = 'https://api-v3.mbta.com'


def get_routes(session, route_type_filter='0,1'):
    """Return routes from API.

    Types parameter can be extended per API spec if a need is identified.
    Default gets heavy rail, light rail.
    """
    url = '/'.join([BASE_API_URL, 'routes'])
    params = {'filter[type]': route_type_filter}  # heavy rail, light rail
    resp = session.get(url, params=params)
    resp.raise_for_status()
    return resp.json()


def get_stops_for_route(session, route_id, sort_by='name'):
    url = '/'.join([BASE_API_URL, 'stops'])
    params = {'filter[route]': route_id, 'sort': sort_by}
    resp = session.get(url, params=params)
    resp.raise_for_status()
    return resp.json()


def get_departure_predictions(session, route_id, stop_id, direction_id):
    url = '/'.join([BASE_API_URL, 'predictions'])
    params = {'filter[route]': route_id,
              'filter[stop]': stop_id,
              'filter[direction_id]': direction_id,
              'sort': 'time'}
    resp = session.get(url, params=params)
    resp.raise_for_status()

    json = resp.json()

    # Filter out arrivals and other noise
    departures = [item for item in json['data'] if item['attributes']['departure_time'] is not None]
    json.update({'data': departures})
    return json


def get_next_departure(session, route_id, stop_id, direction_id):
    """Return time and status (if existing) of next departure as a string.

    There is a mention in API docs that it is possible to return something that
    has departed a few seconds ago.

    In a production system it would be prudent to bring in a battle tested time
    library, convert departure time and system time to timestamps, and do a
    comparison.
    """
    departures = get_departure_predictions(session, route_id, stop_id, direction_id)['data']

    if not departures:  # empty list
        return('No departures found.')

    departure_time = departures[0]['attributes']['departure_time']
    status = departures[0]['attributes']['status']  # None if there is no published status message
    status_msg = f" (Status: {status})" if status is not None else ''

    pattern_in = '%Y-%m-%dT%H:%M:%S%z'
    pattern_out = '%-I:%M%p'

    pretty_time = datetime.strftime(datetime.strptime(departure_time, pattern_in), pattern_out)

    output = f"{pretty_time} Boston time is next departure.{status_msg}"

    return output
