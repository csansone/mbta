# cli.py

from mbta.operations import get_routes, get_stops_for_route, get_next_departure
from mbta.session import create_session


def run_cli():
    session = create_session()

    # Choose route
    routes = get_routes(session)['data']
    route_names = [item['attributes']['long_name'] for item in routes]
    route_index = get_user_choice(route_names, 'route')
    route = routes[route_index]
    route_id = route['id']

    # Choose stop
    stops = get_stops_for_route(session, route_id)['data']
    stop_names = [f"{item['attributes']['name']} ({item['attributes']['municipality']})" for item in stops]
    stop_index = get_user_choice(stop_names, 'stop')
    stop = stops[stop_index]
    stop_id = stop['id']

    # Choose direction
    directions = zip(route['attributes']['direction_names'], route['attributes']['direction_destinations'])
    directions = [f"{item[0]} ({item[1]})" for item in directions]
    direction_index = get_user_choice(directions, 'direction')
    # NOTE: the direction index of 0|1 is used as `direction_id` in predictions endpoint

    next_departure = get_next_departure(session, route_id, stop_id, direction_index)
    print(next_departure)


def get_user_choice(all_choices, label):
    valid = False
    while not valid:
        for i, name in enumerate(all_choices):
            print(f"{i+1 :>4} : {name}")  # +1 for user friendliness
        choice = input(f"Choose the number of your {label} (1-{len(all_choices)}): ")
        valid = is_valid_list_choice(choice, len(all_choices))
        if not valid:
            print('Invalid choice.')
    return int(choice) - 1


def is_valid_list_choice(choice, list_length):
    """Test whether choice meets requirements, return boolean."""
    try:
        choice = int(choice)
    except ValueError:
        return False

    if not 0 < choice <= list_length:
        return False

    return True
