import requests

from mbta.operations import get_routes, get_stops_for_route, get_departure_predictions


session = requests.Session()


def test_get_routes():
    resp = get_routes(session)
    assert 'data' in resp


def test_get_stops_for_route():
    resp = get_stops_for_route(session, 'Red')
    assert 'data' in resp
    assert 'errors' not in resp

    resp = get_stops_for_route(session, 'NONESISTENT')
    assert 'data' in resp
    assert 'errors' not in resp
    assert len(resp.get('data')) == 0


def test_get_departure_predictions():
    resp = get_departure_predictions(session, 'Red', 'place-alfcl', '0')
    assert 'data' in resp
    assert 'errors' not in resp
