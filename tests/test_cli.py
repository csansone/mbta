from mbta.cli import is_valid_list_choice


def test_valid_list_choice_zero_or_negative():
    list_length = 55
    for choice in ['0', '-1', '-99']:
        assert is_valid_list_choice(choice, list_length) is False


def test_valid_list_choice_non_numeric():
    list_length = 5
    for choice in ['a', 'no', '1p', '1o', 'Y', 'qqqqq']:
        assert is_valid_list_choice(choice, list_length) is False


def test_valid_list_choice_too_large():
    list_length = 5
    for choice in ['6', '90']:
        assert is_valid_list_choice(choice, list_length) is False


def test_valid_list_choice_in_range():
    list_length = 5
    for choice in range(1, 6):
        assert is_valid_list_choice(choice, list_length) is True
